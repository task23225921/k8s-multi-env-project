# Setting Up CI/CD Pipelines for Multiple Environments with GitLab

1. Provision three EC2 instances on AWS using your preferred distribution or utilize other cloud environments as per your preference.
![EC2-Instance](images/EC2_Instance.png)
2. Configure security group rules for each instance.
3. Register each runner with GitLab.
4. Install GitLab Runner on each instance. 
![gitlab-runner](images/gitlab-runner.png)
5. Install Kubernetes on your local laptop using a distribution of your choice. Alternatively, you can utilize a cloud environment. Create three clusters to deploy for each environment: development (dev), staging, and production (prod).
![clusters](images/clusters.png)

# Current Architecture for Assignment
![architecture](images/architecture.png)

## Task list 
1. *Installing the Kubernetes Agent*
To integrate a Kubernetes cluster with GitLab, you need to install an agent within your cluster. Follow these steps for installation
* Create an Agent Configuration File:
Generate a configuration file for the agent.
* Register the Agent with GitLab:
Register the agent with GitLab to establish communication.
* Install the Agent in Your Cluster:
Deploy the agent within your Kubernetes cluster.
![gitlab-agent](images/gitlab-agent.png)
2. Creating a Project Runner(dev,staging,prod) with Runner 
![gitlab-runner](images/gitlab-runner.png)
3. CI/CD Pipeline Overview
* The pipeline consists of stages: test, build, deploy_dev, deploy_staging, and deploy_prod.
* Jobs within each stage are executed sequentially.
* Jobs are tagged to specific runners (dev, staging, prod) for execution.
4. Docker Build Stage
* Two Docker build jobs (docker_build_kanban_app and docker_build_kanban_ui) are executed in the build stage.
* They both use Docker images (docker:24.0.6-dind) as the base image and Docker services (docker:24.0.6-dind) for Docker-in-Docker functionality.
5. Deployment Stages
* Three deployment jobs (deploy_to_dev, deploy_to_staging, deploy_to_prod) are executed in the deploy_dev, deploy_staging, and deploy_prod stages, respectively.
* They use the dtzar/helm-kubectl image for Kubernetes operations.
* Each job configures the appropriate Kubernetes context (dev, staging, prod) for deployment.
![gitlab-ci](images/gitlab-ci.png)

## Technology and Tech Stack Using for Assignment

* *AWS* (Infrastructure)
* *Kubernetes* (Container Orchestation)
* *GitLab* (CI/CD)
* *Harbor* (Container Registery)
* *Helm* (Template-based Configuration)
* *Git* (Version Control System)

--------------------
### Verify Docker Push in Harbor Registry
--------------------
![harbor](images/harbor.png)

--------------------
### Verify Job
--------------------
![job-success](images/job-success.png)

--------------------
### Verify Deploy
--------------------
```
kubectl get all -A
``` 

![deploy-test](images/deploy-test1.png)

### To Verify port forwarding
```
kubectl port-forward service/kanban-ui -n my-kanban --address 0.0.0.0 8088:4200
```

![kanban-app](images/kanban-app.png)

